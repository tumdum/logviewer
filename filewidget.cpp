#include "filewidget.h"
#include <QElapsedTimer>
#include <QDebug>
#include <QFile>
#include <QHBoxLayout>
#include <QtConcurrent>
#include <QApplication>
#include <QTextBlock>
#include "filereader.h"
#include "filterdialog.h"
#include "query.h"

FileWidget::FileWidget(const QFileInfo& fileInfo,
                       QWidget *parent,
                       FileView* edit)
    : QWidget(parent), m_fileInfo(fileInfo)
{
    if (!edit)
    {
        qDebug() << "Reading failed";
        return;
    }

    qDebug() << "Reading done";
    m_edit = edit;
    m_edit->setParent(this);
    m_edit->readingDone();

    QHBoxLayout* l = new QHBoxLayout(this);
    l->addWidget(m_edit);
    setLayout(l);
}

void FileWidget::startFilter()
{
    qDebug() << __PRETTY_FUNCTION__;
    FilterDialog dialog;
    if (dialog.exec() == QDialog::Rejected)
    {
        return;
    }

    auto q = dialog.query();
    Query query{q, [q](const QString& s) { return s.contains(q); }};
    m_edit->filter(query);
}


