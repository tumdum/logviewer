#ifndef FILEVIEW_H
#define FILEVIEW_H

#include <QWidget>
#include <QPlainTextEdit>
#include <QTabWidget>
#include <functional>
#include <map>
#include <vector>
#include <QPlainTextDocumentLayout>
#include "query.h"

class FileView : public QWidget
{
    Q_OBJECT
public:
    explicit FileView(const QString& path, QWidget *parent = 0);
    void filter(const Query& q);
    void readingDone();

signals:

public slots:
    void appendText(const QString&);
    void tabChanged(int idx);
    void tabClosed(int idx);
private:
    QPlainTextEdit* m_edit;
    QTabWidget* m_tabs;
    QString m_path;
    std::vector<std::vector<bool>> m_layouts;
};

#endif // FILEVIEW_H
