#ifndef FILEWIDGET_H
#define FILEWIDGET_H

#include <QWidget>
#include <QFileInfo>
#include "fileview.h"

class FileWidget : public QWidget
{
    Q_OBJECT
public:
    explicit FileWidget(const QFileInfo& fileInfo, QWidget *parent, FileView* edit);

    void startFilter();

    QString filePath() const { return m_fileInfo.filePath(); }

signals:

private:
    QFileInfo m_fileInfo;
    FileView* m_edit;
};

#endif // FILEWIDGET_H
