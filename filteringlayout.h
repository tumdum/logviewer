#ifndef FILTERINGLAYOUT_H
#define FILTERINGLAYOUT_H

#include <QPlainTextDocumentLayout>
#include <QTextDocument>
#include <vector>

class FilteringLayout : public QPlainTextDocumentLayout
{
    Q_OBJECT
public:
    FilteringLayout(QTextDocument* doc, std::vector<bool> visibility);
    QRectF blockBoundingRect(const QTextBlock &block) const Q_DECL_OVERRIDE;
    QSizeF documentSize() const Q_DECL_OVERRIDE;

private:
    std::vector<bool> m_visibility;
    QSizeF m_docSize;
};

#endif // FILTERINGLAYOUT_H
