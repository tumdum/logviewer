#include "filterdialog.h"
#include <QDialogButtonBox>
#include <QDebug>

FilterDialog::FilterDialog(QWidget *parent)
    : QDialog(parent), m_layout(new QVBoxLayout(this))
{
    setWindowTitle("Filter");

    m_edit = new QLineEdit(this);
    m_layout->addWidget(m_edit);
    auto buttons = new QDialogButtonBox(this);
    buttons->addButton("Filter", QDialogButtonBox::AcceptRole);
    buttons->addButton("Cancel", QDialogButtonBox::RejectRole);
    m_layout->addWidget(buttons);

    connect(buttons, &QDialogButtonBox::accepted,
            this, &QDialog::accept);
    connect(buttons, &QDialogButtonBox::rejected,
            this, &QDialog::reject);


}

