#include <QApplication>
#include <QPlainTextEdit>
#include <QElapsedTimer>
#include <QDebug>
#include <QTextStream>
#include <QFontDatabase>
#include <QtGui>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>
#include <QRegularExpression>
#include "mainwindow.h"

const QString appName{"LogViewer"};

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QGuiApplication::setApplicationDisplayName(appName);
    QCoreApplication::setApplicationName(appName);
    MainWindow mw;
    mw.show();
    return a.exec();
    /*
    QWidget mw;

    QElapsedTimer timer;
    timer.start();
    QFile file{"/mnt/storage/Pronto/PR178466/IAV OUL TL020226_Syslog_9.log"};
    if (!file.open(QFile::ReadOnly))
    {
        qDebug() << "Failed to open file " << file.fileName();
        return EXIT_FAILURE;
    }
    QTextStream stream{&file};
    const QString content = stream.readAll();
    qDebug() << "read took: " << timer.elapsed() << "ms";

    QVBoxLayout list{&mw};
    // mw.setCentralWidget(&list);

    QPlainTextEdit edit{&mw};
    list.addWidget(&edit);
    QHBoxLayout lineLayout;
    QCheckBox checkBox{"re?", &mw};
    QLineEdit line{&mw};
    QPushButton findBtn{"Find", &mw};
    lineLayout.addWidget(&checkBox);
    lineLayout.addWidget(&line);
    lineLayout.addWidget(&findBtn);
    list.addLayout(&lineLayout);

    QWidget::connect(&findBtn, &QPushButton::pressed,
        [&line, &edit, &checkBox] () {
        qDebug() << "editing finished: " << line.text();
        QTextDocument& doc = *edit.document();
        qDebug() << "block count: " << doc.blockCount();
        int visible = 0;
        QRegularExpression re{line.text()};
        for (QTextBlock it = doc.begin(); it != doc.end(); it = it.next())
        {
            bool v = false;
            if (checkBox.checkState() == Qt::Checked)
            {
                v = re.match(it.text()).hasMatch();
            }
            else
            {
                v = it.text().contains(line.text());
            }
            if (v) visible++;
            it.setVisible(v);
        }
        edit.viewport()->update();
        qDebug() << "Done, visible:" << visible;
    });

    mw.setLayout(&list);
    edit.setReadOnly(true);
    edit.setWordWrapMode(QTextOption::NoWrap);
    edit.document()->setDefaultFont(QFontDatabase::systemFont(QFontDatabase::FixedFont));
    timer.restart();
    edit.setPlainText(content);
    line.setFocus();
    qDebug() << "setPlainText took: " << timer.elapsed() << "ms";
    mw.show();

    return a.exec();*/
}
