#include "filteringlayout.h"
#include <QTextBlock>
#include <QDebug>

FilteringLayout::FilteringLayout(QTextDocument *doc, std::vector<bool> visibility)
    : QPlainTextDocumentLayout(doc), m_visibility(visibility)
{
    qreal w = 0;
    qreal h = 0;

    for (QTextBlock it = doc->begin(); it != doc->end(); it = it.next())
    {
        if (!m_visibility[it.blockNumber()])
        {
            continue;
        }
        const auto r = blockBoundingRect(it);
        if (r.width() > w)
        {
            w = r.width();
        }
        h += r.height();
    }

    m_docSize.setWidth(w);
    m_docSize.setHeight(h);

    emit documentSizeChanged(m_docSize);
}

QRectF FilteringLayout::blockBoundingRect(const QTextBlock &block) const
{
    if (m_visibility[block.blockNumber()])
    {
        return QPlainTextDocumentLayout::blockBoundingRect(block);
    }
    return QRectF();
}

QSizeF FilteringLayout::documentSize() const
{
    qDebug() << __PRETTY_FUNCTION__ << m_docSize;
    return m_docSize;
}
