#include "filereader.h"
#include <QElapsedTimer>
#include <QFile>
#include <QDebug>
#include <QTextDocument>
#include <memory>

namespace
{
const int BLOCK_SIZE = 1024*1024;
}

FileReader::FileReader(const QString& path, QObject* parent)
    : QThread(parent), m_path(path), m_canceled(false)
{

}

void FileReader::cancel()
{
    qDebug() << "Reading" << m_path << "canceled";
    m_canceled = true;
}

void FileReader::run()
{
    QElapsedTimer timer;
    timer.start();
    QFile file{m_path};
    if (!file.open(QFile::ReadOnly))
    {
        qDebug() << "Failed to open file " << file.fileName();
        emit readingDone(false, m_path);
        return;
    }

    const auto size = file.size();

    int total = 0;
    while(total < size)
    {
        if (m_canceled)
        {
            emit readingDone(false, m_path);
            return;
        }
        // reading is much faster then processing...
        usleep(50000);
        const auto array = file.read(BLOCK_SIZE);
        total += array.size();
        emit read(QString(array));
        emit progress(double(total)/double(size)*100);
    }
    qDebug() << "Reading took: " << timer.elapsed();
    emit readingDone(true, m_path);
}
