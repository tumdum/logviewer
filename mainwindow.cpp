#include "mainwindow.h"
#include <QAction>
#include <QMenuBar>
#include <QFileDialog>
#include <QDebug>
#include <QTabWidget>
#include <QFileInfo>
#include <QGuiApplication>
#include <QProgressBar>
#include <QStatusBar>
#include <QApplication>
#include <QStyle>
#include <QDesktopWidget>
#include <QProgressDialog>
#include <QFontMetrics>
#include <QShortcut>
#include "filewidget.h"
#include "filereader.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    createMenus();
    createMainWidget();

    auto shortcut = new QShortcut(Qt::CTRL + Qt::Key_B, this);
    connect(shortcut, &QShortcut::activated,
            this, &MainWindow::filterCurrent);

    setGeometry(
        QStyle::alignedRect(
            Qt::LeftToRight,
            Qt::AlignCenter,
            size(),
            QApplication::desktop()->availableGeometry()
        ));

}

void MainWindow::createMenus()
{
    QAction* exit = new QAction(tr("E&xit"), this);
    exit->setShortcut(Qt::CTRL + Qt::Key_Q);
    exit->setEnabled(true);
    exit->setStatusTip(tr("Exit the application"));
    connect(exit, &QAction::triggered, this, &MainWindow::close);

    m_open = new QAction(tr("&Open"), this);
    m_open->setShortcut(Qt::CTRL + Qt::Key_O);
    m_open->setEnabled(true);
    m_open->setStatusTip(tr("Open file"));
    connect(m_open, &QAction::triggered, this, &MainWindow::openFile);

    auto fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(exit);
    fileMenu->addAction(m_open);
}

void MainWindow::openFile()
{
    const auto fileName =
        QFileDialog::getOpenFileName(
            this, tr("Open log"),".", tr("Logs (*.log)"));
    if (fileName.isEmpty())
    {
        return;
    }
    const QFileInfo fileInfo{fileName};

    const int MAX_PATH_LENGTH_IN_PX = 400;
    const QString pathForUser = QFontMetrics(font())
            .elidedText(fileName, Qt::ElideLeft, MAX_PATH_LENGTH_IN_PX);

    auto dialog = new QProgressDialog(
        "Reading '" + pathForUser +"'", "Cancel", 0, 100, this);
    dialog->setModal(false);
    dialog->setAttribute(Qt::WA_DeleteOnClose);
    dialog->show();

    FileReader* reader = new FileReader(fileInfo.absoluteFilePath(), this);
    m_currentEdit = new FileView(fileInfo.fileName(), this);
    connect(reader, &FileReader::read, m_currentEdit, &FileView::appendText);
    connect(reader, &FileReader::readingDone, this, &MainWindow::readingDone);
    connect(reader, &FileReader::progress, dialog, &QProgressDialog::setValue);
    connect(dialog, &QProgressDialog::canceled, reader, &FileReader::cancel);
    connect(reader, &FileReader::finished, reader, &QObject::deleteLater);
    reader->start();
    m_open->setEnabled(false);
}

void MainWindow::tabChanged(int idx)
{
    auto current = dynamic_cast<FileWidget*>(m_central->widget(idx));
    Q_ASSERT(static_cast<bool>(current));
    setWindowFilePath(current->filePath());
}

void MainWindow::readingDone(bool status, const QString& path)
{
    m_open->setEnabled(true);
    if (!status)
    {
        qDebug() << "Reading '" << path << "' failed";
        delete m_currentEdit;
        m_currentEdit = nullptr;
        return;
    }
    const QFileInfo info{path};

    const auto pos = m_central->addTab(new FileWidget(info, this, m_currentEdit), info.fileName());
    m_central->setTabToolTip(pos, info.absoluteFilePath());
    m_central->setCurrentIndex(pos);
    m_currentEdit = nullptr;
}

void MainWindow::tabClosed(int idx)
{
    qDebug() << "Tab" << idx << "closed";
    QWidget* w = m_central->widget(idx);
    m_central->removeTab(idx);
    w->deleteLater();
}

void MainWindow::filterCurrent()
{
    qDebug() << __PRETTY_FUNCTION__;
    auto current = dynamic_cast<FileWidget*>(m_central->currentWidget());
    if (current)
    {
        current->startFilter();
    }
}

void MainWindow::createMainWidget()
{
    m_central = new QTabWidget(this);
    m_central->setTabBarAutoHide(true);
    m_central->setDocumentMode(true);
    m_central->setTabsClosable(true);
    setCentralWidget(m_central);
    connect(m_central, &QTabWidget::currentChanged,
        this, &MainWindow::tabChanged);
    connect(m_central, &QTabWidget::tabCloseRequested,
            this, &MainWindow::tabClosed);
}
