#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTabWidget>
#include "fileview.h"

class QAction;
class QProgressBar;

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);

signals:

public slots:
    void openFile();
    void tabChanged(int);
    void readingDone(bool status,const QString& path);
    void tabClosed(int idx);

    void filterCurrent();

private:
    void createMenus();
    void createMainWidget();

    QAction* m_open;
    QTabWidget* m_central;
    FileView* m_currentEdit;
};

#endif // MAINWINDOW_H
