#ifndef FILEREADER_H
#define FILEREADER_H

#include <QThread>
#include <QFileInfo>
#include <atomic>

class QTextDocument;

class FileReader : public QThread
{
    Q_OBJECT
public:
    FileReader(const QString& path, QObject* parent = nullptr);
    void run() Q_DECL_OVERRIDE;
signals:
    void progress(int);
    void read(const QString&);
    void readingDone(bool,const QString&);
public slots:
    void cancel();
private:
    const QString m_path;
    std::atomic_bool m_canceled;
};

#endif // FILEREADER_H
