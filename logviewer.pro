#-------------------------------------------------
#
# Project created by QtCreator 2016-11-20T15:30:22
#
#-------------------------------------------------

QT       += core gui concurrent
CONFIG   += C++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = logviewer
TEMPLATE = app

SOURCES += main.cpp mainwindow.cpp \
    filewidget.cpp \
    filereader.cpp \
    filterdialog.cpp \
    fileview.cpp \
    filteringlayout.cpp

HEADERS  += mainwindow.h \
    filewidget.h \
    filereader.h \
    filterdialog.h \
    fileview.h \
    filteringlayout.h \
    query.h
