#ifndef FILTERDIALOG_H
#define FILTERDIALOG_H

#include <QDialog>
#include <QVBoxLayout>
#include <QLineEdit>

class FilterDialog : public QDialog
{
    Q_OBJECT
public:
    explicit FilterDialog(QWidget *parent = 0);
    QString query() { return m_edit->text(); }

signals:

public slots:
private:
    QVBoxLayout* m_layout;
    QLineEdit* m_edit;
};

#endif // FILTERDIALOG_H
