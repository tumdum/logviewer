#include "fileview.h"
#include <QHBoxLayout>
#include <QTextBlock>
#include <QDebug>
#include "filteringlayout.h"

FileView::FileView(const QString& path, QWidget *parent)
    : QWidget(parent), m_edit(new QPlainTextEdit(this)), m_tabs(new QTabWidget(this)), m_path(path)
{
    m_tabs->setTabBarAutoHide(true);
    m_tabs->setTabsClosable(true);
    connect(m_tabs, &QTabWidget::currentChanged,
            this, &FileView::tabChanged);
    connect(m_tabs, &QTabWidget::tabCloseRequested,
            this, &FileView::tabClosed);

    m_edit->setReadOnly(true);
    m_edit->setWordWrapMode(QTextOption::NoWrap);
    m_edit->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    m_edit->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    m_edit->document()->setDefaultFont(QFontDatabase::systemFont(QFontDatabase::FixedFont));
    auto layout = new QHBoxLayout(this);
    layout->addWidget(m_tabs);
    setLayout(layout);
}

void FileView::filter(const Query& q)
{
    std::vector<bool> visibility;
    visibility.resize(m_edit->document()->blockCount(), false);
    size_t i = 0;
    for (QTextBlock it = m_edit->document()->begin(); it != m_edit->document()->end(); it = it.next())
    {
        visibility[i++] = q.predicate(it.text());
    }
    auto edit = new QPlainTextEdit(this);
    auto layout = new FilteringLayout(m_edit->document(), visibility);
    m_edit->document()->setDocumentLayout(layout);
    edit->setDocument(m_edit->document());
    size_t pos = m_tabs->addTab(edit,q.name);
    Q_ASSERT(m_layouts.size() == pos);
    m_layouts.push_back(visibility);
    m_tabs->setCurrentIndex(pos);
    m_edit->viewport()->update();
}

void FileView::readingDone()
{
    std::vector<bool> all;
    all.resize(m_edit->document()->blockCount(), true);
    m_layouts.push_back(all);
    const auto pos = m_tabs->addTab(m_edit, m_path);
    m_tabs->setTabToolTip(pos, m_path);
}

void FileView::appendText(const QString& text)
{
    m_edit->appendPlainText(text);
}

void FileView::tabChanged(int idx)
{
    // Q_ASSERT(m_layouts.find(idx) != m_layouts.end());
    Q_ASSERT(m_layouts.size() > static_cast<size_t>(idx));
    m_edit->document()->setDocumentLayout(new FilteringLayout(m_edit->document(), m_layouts.at(idx)));
    m_edit->viewport()->update();
}

void FileView::tabClosed(int idx)
{
    // Q_ASSERT(m_layouts.find(idx) != m_layouts.end());
    Q_ASSERT(m_layouts.size() > static_cast<size_t>(idx));
    qDebug() << "tabClosed" << idx;
    m_tabs->removeTab(idx);
    auto it = m_layouts.begin();
    std::advance(it, idx);
    m_layouts.erase(it);
}
