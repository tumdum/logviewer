#ifndef QUERY
#define QUERY

#include <QString>
#include <functional>

struct Query
{
    QString name;
    std::function<bool (const QString&)> predicate;
};

#endif // QUERY

